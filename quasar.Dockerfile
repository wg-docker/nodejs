ARG NODE_VERSION

FROM registry.gitlab.com/wg-docker/nodejs:${NODE_VERSION}

RUN yarn global add @quasar/cli